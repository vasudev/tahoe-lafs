
# This _version.py is generated from git metadata by the tahoe setup.py.

__pkgname__ = "tahoe-lafs"
real_version = "1.12.1"
full_version = "ce47f6aaee952bfc9872458355533af6afefa481"
branch = "master"
verstr = "1.12.1"
__version__ = verstr
